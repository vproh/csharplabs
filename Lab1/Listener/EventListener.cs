using System.Collections.Generic;
using System.Linq;
using Lab1.Events;

namespace Lab1.Listener
{
    public class EventListener
    {
        public List<ListEntry> ListenerList { get; set; }

        public EventListener()
        {
            ListenerList = new List<ListEntry>();
        }

        public void AddEvent(object source, MagazineListHandlerEventArgs args)
        {
            ListEntry entry = new ListEntry(args.CollectionName, args.CollectionChangeType, args.ChangedElementNumber);
            ListenerList.Add(entry);
        }

        public override string ToString()
        {
            return string.Format("{0}", string.Join("\n", ListenerList.Select(x => x.ToString()).ToArray()));
        }
    }
}