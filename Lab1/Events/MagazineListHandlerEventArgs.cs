namespace Lab1.Events
{
    public class MagazineListHandlerEventArgs: System.EventArgs
    {
        public string CollectionName { get; set; }
        public string CollectionChangeType { get; set; }
        public int ChangedElementNumber { get; set; }

        public MagazineListHandlerEventArgs() : this(null, null, 0)
        {          
        }
                
        public MagazineListHandlerEventArgs(string collectionName, string collectionChangeType, int elementNumber)
        {
            CollectionName = collectionName;
            CollectionChangeType = collectionChangeType;
            ChangedElementNumber = elementNumber;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}",
                CollectionName, CollectionChangeType, ChangedElementNumber);
        }
    }
}