using System;
using System.Collections.Generic;
using Lab1.Models;

namespace Lab1.Utils
{
    public class TestCollections
    {
        public List<Edition> Editions { get; set; }
        public List<string> Text { get; set; }
        public Dictionary<Edition, Magazine> EdMagDictionary { get; set; }
        public Dictionary<string, Magazine> StMagDictionary { get; set; }

        public static Magazine GenerateMagazineWithIndex(int index)
        {
            Person defaultPerson = GenerateDefaultPerson(index);
            Magazine magazine = new Magazine(
                new Edition(
                    "Edition  " + index,
                    DateTime.Today.AddDays(index),
                    index++),
                "Magazine " + index,
                Frequency.Monthly,
                DateTime.Today.AddDays(index + 1),
                index,
                new List<Person>
                {
                    defaultPerson
                },
                new List<Article>
                {
                    new Article(defaultPerson, "Article " + index++, index++),
                    new Article(defaultPerson, "Article " + index++, index++),
                    new Article(defaultPerson, "Article " + index++, index++)
                });

            return magazine;
        }

        public TestCollections(int count)
        {
            Editions = new List<Edition>();
            Text = new List<string>();
            EdMagDictionary = new Dictionary<Edition, Magazine>();
            StMagDictionary = new Dictionary<string, Magazine>();

            for (int i = 0; i < count; i++)
            {
                Magazine magazine = GenerateMagazineWithIndex(i);
                Edition edition = magazine.Edition;

                Editions.Add(edition);
                Text.Add(edition.ToString());
                EdMagDictionary.Add(edition, magazine);
                StMagDictionary.Add(edition.ToString(), magazine);
            }
        }

        public void MeasureTime()
        {
            int length = Editions.Count - 1;
            int[] indexes = {0, length, length / 2, length + 1};

            foreach (var index in indexes)
            {
                var searcherMagazine = GenerateMagazineWithIndex(index);
                var searcherEdition = searcherMagazine.Edition;

                Console.WriteLine("----------------------------");

                var start = Environment.TickCount;
                var answer = Editions.Contains(searcherEdition);
                var end = Environment.TickCount;
                Console.WriteLine("List edition at index {0}: " + (end - start) + " {1}", index, answer);

                start = Environment.TickCount;
                answer = Text.Contains(searcherEdition.ToString());
                end = Environment.TickCount;
                Console.WriteLine("List edition toString at index {0}: " + (end - start) + " {1}", index, answer);

                start = Environment.TickCount;
                answer = EdMagDictionary.ContainsKey(searcherEdition);
                end = Environment.TickCount;
                Console.WriteLine("Dictionary<Edition, Magazine> key at index {0}: " + (end - start) + " {1}", index,
                    answer);

                start = Environment.TickCount;
                answer = EdMagDictionary.ContainsValue(searcherMagazine);
                end = Environment.TickCount;
                Console.WriteLine("Dictionary<Edition, Magazine> value at index {0}: " + (end - start) + " {1}", index,
                    answer);

                start = Environment.TickCount;
                answer = StMagDictionary.ContainsKey(searcherEdition.ToString());
                end = Environment.TickCount;
                Console.WriteLine("Dictionary<string, Magazine> key at index {0}: " + (end - start) + " {1}", index,
                    answer);

                start = Environment.TickCount;
                answer = StMagDictionary.ContainsValue(searcherMagazine);
                end = Environment.TickCount;
                Console.WriteLine("Dictionary<string, Magazine> value at index {0}: " + (end - start) + " {1}", index,
                    answer);
            }
        }

        private static Person GenerateDefaultPerson(int index)
        {
            return new Person("Person " + index,
                "Person last name  " + index,
                DateTime.Today.AddDays(index));
        }
    }
}