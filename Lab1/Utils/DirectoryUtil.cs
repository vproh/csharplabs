using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lab1.Utils
{
    internal class DirectoryUtil : Watcher
    {
        private string Path { get; set; }
        private string Space { get; set; }
        public static long FullSize { get; private set; }

        protected static object locker = new object();

        public DirectoryUtil(string path)
        {
            Path = path;
            Space = "";
        }

        public void GetAllFilesSize()
        {
            if (!Directory.Exists(Path))
            {
                throw new DirectoryNotFoundException("Directory doesn`t exist");
            }

            lock (locker)
            {
                StringBuilder currentPath = new StringBuilder(Space).Append("   ");

                string[] dirs = Directory.GetDirectories(Path);
                string[] files = Directory.GetFiles(Path);

                FullSize += files.Select(file => new FileInfo(file).Length).Sum();
                
                foreach (var dir in dirs)
                {
                    Console.WriteLine(Space + dir);

                    DirectoryUtil directoryUtil = new DirectoryUtil(dir)
                    {
                        Space = currentPath.ToString()
                    };

                    Thread thread = new Thread(directoryUtil.GetAllFilesSize);
                    thread.Start();
                    Threads.Add(thread);
                }

                foreach (var file in files)
                {
                    FileInfo fileInfo = new FileInfo(file);
                    Console.WriteLine(Space + fileInfo.Name + " (" + fileInfo.Length + ")");
                }
            }
        }
    }
}