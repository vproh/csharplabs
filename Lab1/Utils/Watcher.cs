using System;
using System.Collections.Generic;
using System.Threading;

namespace Lab1.Utils
{
    internal class Watcher
    {
        protected static List<Thread> Threads = new List<Thread>();

        public static void Watch()
        {
            for (int i = 0; i < Threads.Count; i++)
            {
                Threads[i].Join();
            }

            Console.WriteLine("Full size is {0} bytes.", DirectoryUtil.FullSize.ToString());
        }
    }
}
