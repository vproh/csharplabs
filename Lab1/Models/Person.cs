using System;

namespace Lab1.Models
{
    [Serializable]
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public Person() : this("First name", "Last name", DateTime.Now)
        {
        }

        public Person(string firstName, string lastName, DateTime birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }

        public int BirthDateYear
        {
            get { return BirthDate.Year; }
            set { BirthDate = new DateTime(value, BirthDate.Month, BirthDate.Day); }
        }

        public override string ToString()
        {
            return string.Format("FirstName: {0}, LastName: {1}, BirthDate: {2}", FirstName, LastName, BirthDate);
        }

        public virtual string ToShortString()
        {
            return string.Format("FirstName: {0}, LastName: {1}", FirstName, LastName);
        }

        protected bool Equals(Person other)
        {
            return string.Equals(FirstName, other.FirstName)
                   && string.Equals(LastName, other.LastName)
                   && BirthDate.Equals(other.BirthDate);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            
            return obj.GetType() == GetType() && Equals((Person) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = FirstName != null ? FirstName.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ BirthDate.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Person left, Person right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Person left, Person right)
        {
            return !Equals(left, right);
        }

        public virtual object DeepCopy()
        {
            var person = (Person) MemberwiseClone();
            person.BirthDate = new DateTime(BirthDateYear, BirthDate.Month, BirthDate.Day);

            return person;
        }
    }
}