namespace Lab1.Models
{
    public enum Frequency
    {
        Weekly,
        Monthly,
        Yearly
    }
}