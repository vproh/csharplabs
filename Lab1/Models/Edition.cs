using System;
using System.Collections.Generic;

namespace Lab1.Models
{
    [Serializable]
    public class Edition : IComparable, IComparer<Edition>
    { 
        public string EditionName { get; set; }
        public DateTime EditionOut { get; set; }
        private int _circulation;
        
        public int Circulation
        {
            get { return _circulation; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Edition`s circulation must be positive number");
                }

                _circulation = value;
            }
        }

        public Edition()
        {
        }

        public Edition(string editionName, DateTime editionOut, int circulation)
        {
            EditionName = editionName;
            EditionOut = editionOut;
            Circulation = circulation;
        }

        public int Compare(Edition x, Edition y)
        {
            if (x != null && y != null)
            {
                return x.EditionOut.CompareTo(y.EditionOut);
            }

            return 0;
        }

        public override string ToString()
        {
            return string.Format("EditionName: {0}, EditionOut: {1}, Circulation: {2}",
                EditionName, EditionOut, Circulation);
        }

        protected bool Equals(Edition other)
        {
            return string.Equals(EditionName, other.EditionName) && EditionOut.Equals(other.EditionOut);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            
            return obj.GetType() == GetType() && Equals((Edition) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((EditionName != null ? EditionName.GetHashCode() : 0) * 397) ^ EditionOut.GetHashCode();
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
        
            Edition otherEdition = obj as Edition;
            
            if (otherEdition != null) 
                return string.Compare(EditionName, otherEdition.EditionName, StringComparison.Ordinal);
            
            throw new ArgumentException("Compared object is not an Edition type");
        }

        public static bool operator ==(Edition left, Edition right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Edition left, Edition right)
        {
            return !Equals(left, right);
        }

        public virtual object DeepCopy()
        {
            var edition = (Edition) MemberwiseClone();
            
            edition.EditionOut = new DateTime(EditionOut.Year, EditionOut.Month, EditionOut.Day);

            return edition;
        }
    }
}