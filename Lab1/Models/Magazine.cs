using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Lab1.Interfaces;

namespace Lab1.Models
{
    [Serializable]
    public class Magazine : Edition, IRateAndCopy
    {
        public string MagazineName { get; set; }
        public Frequency OutFrequency { get; set; }
        public DateTime OutDate { get; set; }
        public List<Article> Articles { get; set; }
        public List<Person> Editors { get; set; }
        public double Rating { get; private set; }

        public Edition Edition
        {
            get { return this; }
            set
            {
                EditionName = value.EditionName;
                EditionOut = value.EditionOut;
                Circulation = value.Circulation;
            }
        }

        public Magazine() : this(new Edition(), "Magazine", Frequency.Monthly, new DateTime(), 0,
            new List<Person> {new Person()}, new List<Article> {new Article()})
        {
        }

        public Magazine(Edition edition, string magazineName, Frequency outFrequency, DateTime outDate,
            int circulation, List<Person> editors, List<Article> articles)
            : base(edition.EditionName, edition.EditionOut, edition.Circulation)
        {
            MagazineName = magazineName;
            OutFrequency = outFrequency;
            OutDate = outDate;
            Circulation = circulation;
            Articles = articles;
            Rating = ArticlesAverageRating;
            Editors = editors;
        }

        public double ArticlesAverageRating
        {
            get
            {
                if (Articles == null || Articles.Count == 0)
                {
                    return 0;
                }

                var ratingSum = 0.0;
                foreach (Article article in Articles)
                {
                    ratingSum += article.Rating;
                }

                return ratingSum / Articles.Count;
            }
        }

        public bool this[Frequency frequency]
        {
            get { return frequency == OutFrequency; }
        }

        public ArrayList this[int rating]
        {
            get
            {
                var articles = new ArrayList();
                foreach (var article in Articles)
                {
                    if (article.Rating > rating)
                    {
                        articles.Add(article);
                    }
                }

                return articles;
            }
        }

        public ArrayList this[string articleName]
        {
            get
            {
                var articles = new ArrayList();

                foreach (var article in Articles)
                {
                    if (article.Name.ToLower().Contains(articleName.ToLower()))
                    {
                        articles.Add(article);
                    }
                }

                return articles;
            }
        }

        public void AddEditors(params Person[] editors)
        {
            if (editors == null)
            {
                return;
            }

            foreach (var editor in editors)
            {
                Editors.Add(editor);
            }
        }

        public void AddArticles(params Article[] articles)
        {
            if (articles == null)
            {
                return;
            }

            foreach (var article in articles)
            {
                Articles.Add(article);
            }
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine(string.Format(
                "MagazineName: {0}, OutFrequency: {1}, OutDate: {2}, Circulation: {3}, Rating: {4}",
                MagazineName, OutFrequency, OutDate, Circulation, Rating));
            stringBuilder.AppendLine("Articles:");
            foreach (var article in Articles)
            {
                stringBuilder.AppendLine(article.ToString());
            }

            stringBuilder.AppendLine("Editors:");
            foreach (var editor in Editors)
            {
                stringBuilder.AppendLine(editor.ToString());
            }

            return stringBuilder.ToString();
        }

        public bool AddArticleFromConsole()
        {
            try
            {
                string[] input = GetInputDataForArticle();
                int i = 0;

                if (input.Length % 5 != 0)
                {
                    throw new Exception("Incorrect data has been entered. Please check format and try again.");
                }

                while (i < input.Length)
                {
                    string editorName = input[i++];
                    string editorLastName = input[i++];
                    DateTime editorBirthDate = DateTime.ParseExact(input[i++], "dd.MM.yyyy", null);
                    string articleName = input[i++];
                    double articleRating = Convert.ToDouble(input[i++]);


                    Person editor = new Person(editorName, editorLastName, editorBirthDate);
                    AddEditors(editor);
                    AddArticles(new Article(editor, articleName,
                        articleRating));
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool Save(string filename)
        {
            try
            {
                using (FileStream stream = new FileStream(filename, FileMode.OpenOrCreate))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool Load(string filename)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Open))
            {
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    Magazine magazine = (Magazine) formatter.Deserialize(stream);

                    MagazineName = magazine.MagazineName;
                    OutFrequency = magazine.OutFrequency;
                    OutDate = magazine.OutDate;
                    Circulation = magazine.Circulation;
                    Articles = magazine.Articles;
                    Editors = magazine.Editors;
                    Edition = magazine.Edition;

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
        }

        public static bool Save(string filename, Magazine obj)
        {
            return obj.Save(filename);
        }

        public static bool Load(string filename, Magazine obj)
        {
            return obj.Load(filename);
        }

        private string[] GetInputDataForArticle()
        {
            Console.WriteLine("Please enter info about new article in the following format:\n" +
                              "EditorName EditorLastName EditorBirthDate(dd.mm.yyyy) " +
                              "ArticleTitle ArticleRating.\n" +
                              "Use space as text splitter");

            string inputData = Console.ReadLine();

            return inputData == null ? null : inputData.Split(' ');
        }

        public virtual string ToShortString()
        {
            return string.Format("Name: {0}," +
                                 " OutFrequency: {1}," +
                                 " OutDate: {2}," +
                                 " Circulation: {3}," +
                                 " Average articles rating: {4}",
                MagazineName,
                OutFrequency,
                OutDate,
                Circulation,
                ArticlesAverageRating);
        }

        protected bool Equals(Magazine other)
        {
            return string.Equals(MagazineName, other.MagazineName) && OutDate.Equals(other.OutDate);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;

            return obj.GetType() == GetType() && Equals((Magazine) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = MagazineName != null ? MagazineName.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ OutDate.GetHashCode();

                return hashCode;
            }
        }

        public static bool operator ==(Magazine left, Magazine right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Magazine left, Magazine right)
        {
            return !Equals(left, right);
        }

        public override object DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                if (!GetType().IsSerializable) return null;

                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;

                return formatter.Deserialize(stream);
            }
        }
    }
}